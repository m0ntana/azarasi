#!/usr/bin/php
<?php
require_once("class/snmpq.class.php");

if(count($argv) < 3) {
    echo $argv[0] . " <ip/host> <community>\n";
    exit(1);
}

$host = $argv[1];
$community = $argv[2];


//$oids = array();
$snmp = new SNMPQ();
$data = $snmp->walk($host, $community, "IF-MIB::ifName");

$i = 1;
foreach($data as $ifname) {
    echo $ifname . ":\n";
    echo "IF-MIB::ifHCInOctets." . $i . "\n";
    echo "IF-MIB::ifHCOutOctets." . $i . "\n";
    echo "\n";
    $i++;
}
?>