#!/usr/bin/php
<?php
require_once("class/db.class.php");
require_once("class/poller.class.php");
require_once("class/snmpq.class.php");
require_once("class/templates.class.php");

class ConfigReader {
    private static $CONFIG = false;
    public static $CONFIGFILE = "config.ini";
    
    
    public static function get() {
	if(!ConfigReader::$CONFIG)
	    ConfigReader::init();
	
	return ConfigReader::$CONFIG;
    }

    public static function print_hosts() {
	ConfigReader::init();
	foreach(ConfigReader::$CONFIG["datasource"] as $name => $values)
	    echo $name . "\n";
    }

    private static function init($filename = false) {
	if($filename)
	    ConfigReader::$CONFIGFILE = $filename;
	
	$data = parse_ini_file(ConfigReader::$CONFIGFILE, true);
	if(!$data) {
	    echo "Can't read config: " . ConfigReader::$CONFIGFILE . "\n";
	    exit(1);
	}
	
	$config = array("poller" => array(), "datasource" => array());
	
	foreach($data as $section => $values) {
	    if($section == "poller")
		$config["poller"] = $values;
	    else
		$config["datasource"][$section] = $values;
	}
	
	ConfigReader::$CONFIG = $config;
    }
}


function help($argv) {
    echo "Usage: " . $argv[0] . " [options]\n\n";
    echo "\t--tpls   - list query templates for grafana\n";
    echo "\t--tpl    - select template for grafana\n";
    echo "\t--hosts  - list all hosts known\n";
    echo "\t--poller - just run poller and collect data from hosts\n";
    echo "\t-h       - set host from config.ini (specify multiple times if needed)\n";
}

$s_opts = "h:";
$opts = array(
    "tpls",
    "tpl:",
    "poller",
    "hosts",
    "help",
);

$options = getopt($s_opts, $opts);

if(empty($options))
    help($argv);

if(isset($options["tpls"]))
    return Templates::list();

if(isset($options["tpl"]) && isset($options["h"]))
    return Templates::show($options["tpl"], $options["h"]);

if(isset($options["hosts"])) {
    echo "Hosts configured:\n";
    ConfigReader::print_hosts();
}

if(isset($options["poller"]))
    Poller::run(isset($options["h"])? $options["h"] : array());
?>