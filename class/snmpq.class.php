<?php
class SNMPQ {
    public function get($host, $community, $oid) {
	snmp_set_quick_print(TRUE);
	$data = snmp2_get($host, $community, $oid);
	$data = trim($data, "\"");
	
	return $data;
    }
    
    public function walk($host, $community, $obj) {
	snmp_set_quick_print(TRUE);
	$data = snmp2_walk($host, $community, $obj);
	
	return $data;
    }
}
?>