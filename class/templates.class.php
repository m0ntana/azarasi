<?php
class Templates {
    private static $MYDIR = "templates";
    
    static function list() {
	$records = scandir(Templates::$MYDIR);
	
	echo "Available templates:\n";
	
	foreach($records as $r) {
	    if($r == "." || $r == "..")
		continue;
	    echo "\t" . $r . "\n";
	}
    }
    
    static function show($tplname, $hosts) {
	$i = new Templates();
	$i->_show($tplname, $hosts);
    }
    
    private function _show($tplname, $hosts) {
	$filename = self::$MYDIR . "/"  . $tplname;

	$tpldata = file_get_contents($filename);
	if(!$tpldata)
	    return false;
	
	if(!is_array($hosts))
	    $hosts = array($hosts);
	
	foreach($hosts as $host) {
	    $tpl = str_replace("_NAME_", $host, $tpldata);
	    echo "\n".$tpl."\n";
	}
    }
}
?>