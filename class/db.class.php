<?php
class DB {
    private $lasterror;
    private $affected_rows;
    private static $sInstance = false;

    public static function getInstance() {
        if(!DB::$sInstance) {
            DB::$sInstance = new DB();
        }
        
        return DB::$sInstance;
    }

    function __construct() {
        $this->conn = false;
    }

    function __destruct() {
        if(!$this->is_connected())
            return;
        
        @pg_close($this->conn);
    }

    private function open_connection() {
        if($this->is_connected())
            return true;
        
        $config = ConfigReader::get();
        $config = $config["poller"];
        
        $this->conn = pg_connect($config["pg_conn_str"]);
        return $this->is_connected();
    }
    
    private function is_connected() {
        return (@pg_connection_status($this->conn) === PGSQL_CONNECTION_OK);
    }

    private function safety($s) {
        $s = str_replace(";", "", $s);
        return $s;
    }

    private function debug($s) {
        //error_log("Q: ".$s."\n");
    }

    public function is_data($data) {
        if(!is_array($data) || count($data) < 1)
            return false;
        
        return true;
    }

    public function lastError() {
        return $this->lasterror;
    }
    
    public function affected() {
        return $this->affected_rows;
    }

    public function cacheQuery($q, $timeout = 600, $safe = false) {
        $key = "sql_" . md5($q);
        $data = $this->mc->get($key);
        if(Config::$DEBUG)
            $data = false;

        if($data === false) {
            error_log("from db");
            $data = $this->query($q, $safe);
            $this->mc->set($key, $data, false, $timeout);
        }
        
        return $data;
    }

    public function qq($what, $to, $q) {
        $q = str_replace($what, $to, $q);
        return $this->query($q);
    }

    public function query($q, $safe = false) {
        $this->debug($q);

        $data = array();

        if(!$this->open_connection()) {
            error_log("DB ERROR: not connected");
            return false;
        }
        
        $result = pg_query($this->conn, $q);

        if(is_bool($result))
            return $result;

        for($i = 0; $i < pg_num_rows($result); $i++) {
            $row = pg_fetch_assoc($result);
            if($row === FALSE)
                break;
            $data[] = $row;
        }

        return $data;
    }
}
?>