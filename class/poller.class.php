<?php
class Poller {
    private $Q1 = "insert into simpledata(name, values) values('_NAME_', '_VALUES_')";

    public function __construct() {
	$this->config = ConfigReader::get();
	$this->snmp = new SNMPQ();
    }
    
    public static function run() {
	$i = new Poller();
	$i->_run();
    }
    
    private function _run() {
	$db = DB::getInstance();
	
	foreach($this->config["datasource"] as $name => $params) {
	    $values = $this->_query_host($params);
	    $values = json_encode($values);
	    
	    $db->qq(array("_NAME_", "_VALUES_"), array($name, $values), $this->Q1);
	}
    }
    
    private function _query_host($params) {
	$values = array();
	foreach($params["oids"] as $oid)
	    $values[] = $this->snmp->get($params["host"], $params["community"], $oid);
	
	return $values;
    }
}
?>