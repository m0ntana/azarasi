select 
    (val0/period)::bigint as IN, 
    (val1/period)::bigint as OUT,
    added as time
from
    (
    select name,added,
       COALESCE(val0 - lag(val0) over (order by added), 0) as val0,
       COALESCE(val1 - lag(val1) over (order by added), 0) as val1,
       COALESCE(date_part('epoch', added - lag(added) over (order by added)), 1) as period
    from
    (select * from v_values_2 where name='_NAME_' and $__timeFilter(added)) t1
    ) t2
;
