create table simpledata(id serial, 
			name text not null, 
			values json not null default '[]', 
			added timestamp with time zone default now());

create index i_simpledata_name on  simpledata (name);
create index i_simpledata_added on simpledata (added);

create view v_values_2 as 
    select id,
	   name,
	   (values->>0)::bigint as val0, 
	   (values->>1)::bigint as val1,
	   added 
    from simpledata;

create view v_values_2_diff as 
    select id,
	   name,
	   coalesce(val0 - lag(val0) over (order by name,added),0) as val0,
	   coalesce(val1 - lag(val1) over (order by name,added),0) as val1,
	   added 
    from v_values_2 ;
